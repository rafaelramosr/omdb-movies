# omdb-movies

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Contenido del proyecto

### Descripción
El proyecto consta de un buscador de películas usando mediante OMDb API. Al entrar se encontrará con una página de inicio donde podrá realizar las búsquedas, la barra de búsqueda responde a la interacción del teclado y el evento click del botón "Buscar", también se permite dar seleccionar una película y ver una información más detallada sobre la misma.

### Estructura
El proyecto, aparte de los archivos de configuración, se divide en las siguientes carpetas:

- `assets`: Carpeta con archivos estáticos para una interfaz más amigable.
- `components`: Contiene los componentes necesarios para cumplir con la prueba:
    - `MovieCard`: Muestra la imagen, título y año de publicación de la película, también se encarga de asignar una imagen de apoyo a aquellas películas que no cuentan con una.
    - `MoviePanel`: Escucha el cambio del *query string* de búsqueda y busca las películas que coincidan con el término de búsqueda, si no hay resultados se muestra un mensaje al usuario de lo contrario se listan las películas.
    - `NotFound`: Muestra un mensaje cuando no se encuentran las películas o un mensaje de bienvenida dependiendo de la ruta.
    - `searchBar`: Se encarga de crear el *slug* de búsqueda.
- `css`: Estilos globales del proyecto.
- `pages`: Contiene las vistas del proyecto:
    - `Home`: Vista de inicio donde se hacen las búsquedas.
    - `Movie`: Vista donde se muestra la información detallada de cada película.
- `routes`: Maneja la navegación del proyecto.
- `utils`: Contiene funciones de ayuda de conexión a la API.

### Librerías y frameworks
- (Vue 3)[https://v3.vuejs.org/]
- (Vue Router v4)[https://next.router.vuejs.org/]

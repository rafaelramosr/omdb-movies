import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    component: () => import('../pages/Home.vue'),
  },
  {
    name: 'movie',
    path: '/movie/:movieId',
    component: () => import('../pages/Movie.vue'),
  },
  {
    name: '404',
    path: '/:catchAll(.*)',
    component: () => import('../pages/Home.vue'),
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export { router };

const API_URL = 'http://www.omdbapi.com';
const API_KEY = '6cb6ad1f';

export const fetchData = async (restQuery) => {
  const response = await fetch(`${API_URL}?apikey=${API_KEY}${restQuery}`);
  const movieData = await response.json();
  const result = movieData.Response === 'True' ? movieData : null;

  return result;
};

export const getAllMovies = async (searchMovie, page = 1) => {
  const movies = await fetchData(`&s=${searchMovie}&page=${page}&type=movie`);
  return movies;
};

export const getOneMovie = async (imdbID) => {
  const movie = await fetchData(`&i=${imdbID}`);
  return movie;
};
